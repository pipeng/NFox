﻿using Microsoft.Win32;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFox.Basal
{
    /// <summary>
    /// 注册表扩展函数
    /// </summary>
    public static class RegEX
    {
        /// <summary>
        /// 打开注册表项
        /// </summary>
        /// <param name="root">根键</param>
        /// <param name="keys">子项逐级列表</param>
        /// <example>GetRegistryKey(root, sub1, sub2, sub3) === root/sub1/sub2/sub3</example>
        /// <returns></returns>
        public static RegistryKey GetRegistryKey(this RegistryKey root, params string[] keys)
        {
            RegistryKey rk = root;
            foreach (string key in keys)
            {
                try
                {
                    rk = rk.OpenSubKey(key);
                }
                catch
                {
                    return null;
                }
            }
            return rk;
        }
    }
}
