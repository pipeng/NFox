﻿using NFox.Runtime.Dll;
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;

namespace NFox.Runtime
{
    public static class InvokeApi
    {
        internal static AssemblyBuilder GetAssemblyBuilder()
        {
            return
                AppDomain.CurrentDomain.DefineDynamicAssembly(
                    new AssemblyName("InvokeApi"),
                    AssemblyBuilderAccess.Run);
        }

        public static Library GetLibrary(string fileName)
        {
            var lib = new Library(fileName, CallingConvention.Winapi);
            if (lib.OK)
                return lib;
            return null;
        }

        public static Library GetLibrary(string fileName, CallingConvention callingConv)
        {
            var lib = new Library(fileName, callingConv);
            if (lib.OK)
                return lib;
            return null;
        }
    }
}