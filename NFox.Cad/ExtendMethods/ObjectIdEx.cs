﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using System.Collections.Generic;
using System.Linq;

namespace NFox.Cad
{
    /// <summary>
    /// 对象id扩展类
    /// </summary>
    public static class ObjectIdEx
    {
        #region GetObject

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <param name="id">对象id</param>
        /// <param name="tr">事务</param>
        /// <param name="mode">打开模式</param>
        /// <param name="openErased">打开删除对象</param>
        /// <returns>DBObject对象</returns>
        public static DBObject GetObject(this ObjectId id, Transaction tr, OpenMode mode = OpenMode.ForRead, bool openErased = false)
        {
            return tr.GetObject(id, mode, openErased);
        }

        /// <summary>
        /// 获取指定类型对象
        /// </summary>
        /// <typeparam name="T">指定的泛型</typeparam>
        /// <param name="id">对象id</param>
        /// <param name="tr">事务</param>
        /// <param name="mode">打开模式</param>
        /// <param name="openErased">打开删除对象</param>
        /// <returns>指定类型对象</returns>
        public static T GetObject<T>(this ObjectId id, Transaction tr, OpenMode mode = OpenMode.ForRead, bool openErased = false) where T : DBObject
        {
            return tr.GetObject(id, mode, openErased) as T;
        }

        /// <summary>
        /// 获取指定类型对象集合
        /// </summary>
        /// <typeparam name="T">指定的泛型</typeparam>
        /// <param name="ids">对象id集合</param>
        /// <param name="tr">事务</param>
        /// <param name="mode">打开模式</param>
        /// <param name="openErased">打开删除对象</param>
        /// <returns>指定类型对象集合</returns>
        public static IEnumerable<T> GetObject<T>(this IEnumerable<ObjectId> ids, Transaction tr, OpenMode mode = OpenMode.ForRead, bool openErased = false) where T : DBObject
        {
            return ids.Select(id => id.GetObject<T>(tr, mode, openErased));
        }
        /// <summary>
        /// 获取指定DBObject对象集合
        /// </summary>
        /// <param name="ids">对象id集合</param>
        /// <param name="tr">事务</param>
        /// <param name="mode">打开模式</param>
        /// <param name="openErased">打开删除对象</param>
        /// <returns>DBObject对象集合</returns>
        public static IEnumerable<DBObject> GetObject(this IEnumerable<ObjectId> ids, Transaction tr, OpenMode mode = OpenMode.ForRead, bool openErased = false)
        {
            return ids.Select(id => id.GetObject(tr, mode, openErased));
        }
        
        /// <summary>
        /// 返回符合类型的对象id
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="ids">对象id集合</param>
        /// <returns>对象id集合</returns>
        public static IEnumerable<ObjectId> OfType<T>(this IEnumerable<ObjectId> ids) where T : DBObject
        {
            string dxfName = RXClass.GetClass(typeof(T)).DxfName;
            return
                ids
                .Where(id => id.ObjectClass.DxfName == dxfName);
        }
        #endregion GetObject




    }
}